import controllers.VeiculoController;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        VeiculoController veiculoController = new VeiculoController();

        veiculoController.registrarVeiculo("MKD-2333");
        veiculoController.removerVeiculo("MKD-2333");
    }
}
