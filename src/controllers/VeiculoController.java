package controllers;

import models.Locacao;
import models.Ticket;

import java.util.ArrayList;

public class VeiculoController {
    private ArrayList<Locacao> locacoes;

    public VeiculoController() {
        this.locacoes = new ArrayList<>();
    }

    public void registrarVeiculo(String placa) {
        Locacao locacao = new Locacao(placa);
        locacao.definirEntrada();
        this.locacoes.add(locacao);
    }

    /**
     * Retorna uma locação com base a placa
     * @param placa
     */
    public Locacao buscaLocacaoPorPlaca(String placa) {
        if (this.locacoes.isEmpty()) return null;

        for (Locacao locacao: this.locacoes) {

            // Verifica se a placa do carro é a mesma
            // se for a mesma retorna a locação
            if (locacao.getPlacaCarro().equals(placa)) {
                return locacao;
            }
        }

        return null;
    }

    public void removerVeiculo(String placa) {
        Locacao locacao = this.buscaLocacaoPorPlaca(placa);
        locacao.definirSaida();
    }

    public void listarLocacoes() {
        for (Locacao locacao: this.locacoes) {
            System.out.println(locacao.gerarTicket());
        }
    }
}
