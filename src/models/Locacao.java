package models;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;

public class Locacao {
    static public final double valorHora = 2.00;

    private Ticket ticket;
    private Veiculo veiculo;
    private LocalDateTime dataEntrada, dataSaida;

    public Locacao(String placa) {
        this.veiculo = new Veiculo(placa);
        this.ticket = new Ticket();
    }

    public void definirEntrada() {
        this.dataEntrada = LocalDateTime.now();
        ticket.setDataEntrada(dataEntrada);
        System.out.println(gerarTicket());
    }

    public void definirSaida() {
        this.dataSaida = LocalDateTime.now();
        ticket.setDataSaida(dataSaida);
        ticket.setValor(this.getValor());
        System.out.println(gerarTicket());
    }

    public double getValor() {
        // 3 horas a menos na data de entrada
        // para poder mostrar o valor a pagar
        // não queria colocar isso no construtor por que seria possivelmente um risco
        Duration duration = Duration.between(this.dataEntrada.minusHours(3), this.dataSaida);
        long diff = Math.abs(duration.toHours());
        return Locacao.valorHora * diff;
    }

    public String getPlacaCarro() {
        return this.veiculo.getPlaca();
    }

    public String gerarTicket() {
        return this.ticket.toString();
    }
}
