package models;

import java.text.NumberFormat;
import java.time.LocalDateTime;

public class Ticket {
    private double valor;
    private LocalDateTime dataEntrada, dataSaida;

    public void setValor(double valor) {
        this.valor = valor;
    }

    public String getValor() {
        return NumberFormat.getCurrencyInstance().format(valor);
    }

    public void setDataEntrada(LocalDateTime dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public void setDataSaida(LocalDateTime dataSaida) {
        this.dataSaida = dataSaida;
    }

    private boolean temDataSaida() {
        return dataSaida != null;
    }

    @Override
    public String toString() {
        String ticket = "";

        if (this.temDataSaida()) {
            ticket += "-------- Ticket de saida --------\n";

            ticket += "Data Entrada: " + dataEntrada.toString() + "\n";
            ticket += "Data Saida: " + dataSaida.toString() + "\n";
            ticket += "Valor: " + this.getValor() + "\n";
            ticket += "---------------------------------\n";
            return ticket;
        }

        ticket += "------- Ticket de entrada -------\n";
        ticket += "Data Entrada: " + dataEntrada.toString() + "\n";
        ticket += "---------------------------------\n";

        return ticket;
    }
}
